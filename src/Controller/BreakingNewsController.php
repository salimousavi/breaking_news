<?php

namespace Drupal\breaking_news\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use \Drupal\taxonomy\Entity\Term;

/**
 * Created by PhpStorm.
 * User: kasper
 * Date: 1/31/17
 * Time: 11:11 AM
 */
class BreakingNewsController extends ControllerBase {

    public function getData() {

        $query = \Drupal::entityQuery('taxonomy_term')
            ->condition('vid', 'breaking_news')
            ->condition('field_breaking_news_enable', 1)
            ->sort('tid', 'DESC')
            ->range(0, 1);

        $result = $query->execute();

        if (!empty($result)) {

            $tid = reset($result);
            $term = Term::load($tid);
            $change = $term->getChangedTime();
            $show_time = $term->get('field_show_time')->getValue()[0]['value'];

            if (time() - $change < $show_time) {
                $result = $term->get('field_breaking_news')->getValue()[0]['value'];
            }
            else {
                $result = null;
            }
        }
        else {
            $result = null;
        }

        return new JsonResponse($result);
    }
}