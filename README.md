# BreakingNews

<h3> Installation </h3>

Download this module and add in module directory and enable it.

<h3> How to work? </h3>
This module create a vocabulary that named <code>breaking_news</code>.<br>

<b>Fields:</b>
 <br><code> field_breaking_news_enable </code>
 <br><code> field_show_time </code>
 <br><code> field_breaking_news </code>

<b>Route request:</b>
 <br>
 <code> api/breaking_news </code>

<b>Response:</b>
 <br>
 <code>[]</code> or <code>[Text of breaking news]</code>
 
 
